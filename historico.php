<?php

include 'bd.php';

$sql = "SELECT data, duracao, erros, acertos, jogadas FROM Partida";
$result = $conn->query($sql);

?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Jogo da memória</title>
    <link rel="stylesheet" href="style.css">
</head>
<body>
   <h1 class="page-title">Histórico de Partidas</h1>   
   <div id="game-container" class="game-container">        
       
    <div class="menu-jogo" id="menu-jogo">
        <ul>
           <!--Histórico aqui!-->
           <?php
             if ($result->num_rows > 0) { 
                while($row = mysqli_fetch_assoc($result)) {              
                
           ?>
           <li>
               <?php 
               echo "Data: " . $row["data"]. " - Duração: " . $row["duracao"]. " - Erros: " . $row["erros"]. " - Acertos: " . $row["acertos"]. " - Jogadas: " . $row["jogadas"];
                             
               ?>

           </li>

           <?php
             }
            }else {
                echo "Sem partidas";
            }
            
           ?>


            

        </ul>

        <a href="/jogo-memoria">Voltar</a>
    
    </div>
          

   </div>
  
</body>
</html>