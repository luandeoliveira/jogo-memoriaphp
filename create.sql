CREATE TABLE Partida (
    idPartida INT AUTO_INCREMENT PRIMARY KEY, 
    data DATETIME, 
    duracao SMALLINT, 
    erros SMALLINT,
    acertos SMALLINT, 
    jogadas SMALLINT 
);
